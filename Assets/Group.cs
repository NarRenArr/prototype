﻿public class Group : IClonable
{
    public int course;
    public string spec;


    public Group(int course, string spec)
    {
        this.course = course;
        this.spec = spec;
    }

    public object Clone()
    {
        return MemberwiseClone();
    }
}
