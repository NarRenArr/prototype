﻿public class Student : IClonable
{
    public string name;
    public string surname;
    public Group group;
    public int age;

    public Student(string name, string surname, Group group, int age)
    {
        this.name = name;
        this.surname = surname;
        this.group = group;
        this.age = age;
    }

    public object Clone()
    {
        return new Student(name, surname, (Group)group.Clone(), age);
    }
}
