﻿using UnityEngine;

public class Program : MonoBehaviour
{
    void Start()
    {
        Student student = new Student("Ихорь", "Катамаранов", new Group(4, "Творческое объединение 'Среднее качество'"), 42);
        Student student2 = (Student)student.Clone();

        student2.group.course++;

        Debug.Log(student.group.course);
        Debug.Log(student2.group.course);
    }
}
